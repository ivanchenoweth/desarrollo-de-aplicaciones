# Motor Control Application

This application is a small didactic desktop application to simulate the control of an electic motor, with a login system, with the CRUD of user and show the logs activity of the motor, using the best practice of developing.

  - Login system
  - CURD of users for administrator
  - Logs options for administrator
  - Turn on the motor
  - Turn off
  - Turn clockwise
  - Turn conter clockwise

### Future Features:

  - Connect the motor to the PC with a microcontroller interfece

### Best practices of software engeneer:
* OOP, the oriented object programing
* MVC, the architectural pattern commonly used to the layers Model, View, Controller
* Repository pattern (used to access to the Models)
* Service Layer pattern [used to o organize the services and keep the skinny the controllers and models]
(https://medium.com/the-falconry/skinny-models-skinny-controllers-fat-services-e04cfe2d6ae)
* Unit test for each public method
* Dependency Injection pattern is used in almost every framework and is based on the "inversion of control" (IoC) principle.
* S.O.L.I.D principle
